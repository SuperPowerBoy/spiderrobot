﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpiderRobot.Domain.Entities;
using SpiderRobot.Domain.Model;
using SpiderRobot.Services;

namespace SpiderRobot.Tests
{
    [TestClass]
    public class SpiderTest
    {

        private SpiderWallViewModel spiderWallViewModel;

        [TestInitialize]
        public void Initialize()
        {
            spiderWallViewModel = new SpiderWallViewModel();
            spiderWallViewModel.MaxCoordinates = "7 15";
            spiderWallViewModel.CurrentLocation = "2 4 Left";
            spiderWallViewModel.InstructionsText = "FLFLFRFFLF";
        }

        [TestMethod]
        public void NavigateTest_givenOutput()
        {
            //arrange
            var spiderService = new SpiderService(new Spider(), new Wall());
            spiderService.SpiderWallViewModel = spiderWallViewModel;

            //act
            var output = spiderService.Navigate();

            //assert
            Assert.AreEqual("3 1 RIGHT", output);

        }

    }
}
