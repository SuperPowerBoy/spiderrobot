﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpiderRobot;
using SpiderRobot.Controllers;
using SpiderRobot.Domain.Entities;
using SpiderRobot.Services;
using SpiderRobot.Services.Interfaces;

namespace SpiderRobot.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        private ISpiderService _spiderService;

        [TestInitialize]
        public void Initialize()
        {
            _spiderService = new SpiderService(new Spider(), new Wall());
        }

        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController(_spiderService);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

    }
}
