﻿using System;
using System.Collections.Generic;

namespace SpiderRobot.Models
{
    public class Spider
    {
        //Input
        public string _maxCoordinates { get; set; }
        public string _currentLocation { get; set; }
        public string _instructions { get; set; }

        public int _XCoordinate { get; set; }
        public int _YCoordinate { get; set; }
        public string _Orientation { get; set; }

        public Wall Wall { get; set; }

        public Spider()
        {
            Wall = new Wall();
        }

        public void Navigate()
        {
            var maxCoordinates = _maxCoordinates.Split(' ');
            if (_maxCoordinates.Split(' ').Length == 2)
            {
                Wall.XMaxCoordinate = int.Parse(maxCoordinates[0]);
                Wall.YMaxCoordinate = int.Parse(maxCoordinates[1]);
            }

            if (_currentLocation.Split(' ').Length == 3)
            {
                _XCoordinate = int.Parse(_currentLocation.Split(' ')[0]);
                _YCoordinate = int.Parse(_currentLocation.Split(' ')[1]);
                _Orientation = _currentLocation.Split(' ')[2].ToString().ToUpper();

                var instructionList = _instructions.ToCharArray();

                foreach (var instruction in instructionList)
                {
                    Respond(instruction.ToString().ToUpper());
                }

            }
        }

        private void Respond(string instruction)
        {
            switch (instruction)
            {
                case "L":
                    _Orientation = GetOrientation(_Orientation, "L");
                    break;
                case "R":
                    _Orientation = GetOrientation(_Orientation, "R");
                    break;
                case "F":
                    switch (_Orientation)
                    {
                        case "UP":
                            if (_YCoordinate < Wall.YMaxCoordinate)
                            {
                                _YCoordinate++;
                            }
                            else
                            {
                                throw new Exception("Out of bounds");
                            }
                            break;
                        case "DOWN":
                            if (_YCoordinate > Wall.YMinCoordinate)
                            {
                                _YCoordinate--;
                            }
                            else
                            {
                                throw new Exception("Out of bounds");
                            }
                            break;
                        case "LEFT":
                            if (_XCoordinate > Wall.YMinCoordinate)
                            {
                                _XCoordinate--;
                            }
                            else
                            {
                                throw new Exception("Out of bounds");
                            }
                            break;
                        case "RIGHT":
                            if (_XCoordinate < Wall.YMaxCoordinate)
                            {
                                _XCoordinate++;
                            }
                            else
                            {
                                throw new Exception("Out of bounds");
                            }
                            break;
                    }
                    break;
            }
        }

        private string GetOrientation(string currentOrientation, string instruction)
        {
            string orientation = String.Empty;
            switch (instruction)
            {
                case "L":
                    switch (currentOrientation.ToUpper())
                    {
                        case "LEFT":
                            orientation = "DOWN";
                            break;
                        case "DOWN":
                            orientation = "RIGHT";
                            break;
                        case "UP":
                            orientation = "LEFT";
                            break;
                        case "RIGHT":
                            orientation = "UP";
                            break;
                    }
                    break;
                case "R":
                    switch (currentOrientation.ToUpper())
                    {
                        case "LEFT":
                            orientation = "UP";
                            break;
                        case "DOWN":
                            orientation = "LEFT";
                            break;
                        case "UP":
                            orientation = "RIGHT";
                            break;
                        case "RIGHT":
                            orientation = "DOWN";
                            break;
                    }
                    break;
            }

            return orientation;
        }
    }
}
