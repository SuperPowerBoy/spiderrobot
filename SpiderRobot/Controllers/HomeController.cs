﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpiderRobot.Domain.Entities;
using SpiderRobot.Domain.Model;
using SpiderRobot.Services;
using SpiderRobot.Services.Interfaces;

namespace SpiderRobot.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISpiderService _spiderService;

        public HomeController(ISpiderService spiderService)
        {
            _spiderService = spiderService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(SpiderWallViewModel model)
        {
            _spiderService.SpiderWallViewModel = model;
            model.Output = _spiderService.Navigate();

            return View(model);
        }
        
    }
}