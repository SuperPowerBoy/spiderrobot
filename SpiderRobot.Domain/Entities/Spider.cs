﻿namespace SpiderRobot.Domain.Entities
{
    public class Spider
    {
        public int XCoordinate { get; set; }
        public int YCoordinate { get; set; }
        public string Orientation { get; set; }

    }
}
