﻿namespace SpiderRobot.Domain.Entities
{
    public class Wall
    {
        public int XMaxCoordinate { get; set; }
        public int YMaxCoordinate { get; set; }

        public int XMinCoordinate
        {
            get { return 0; }
        }

        public int YMinCoordinate
        {
            get { return 0; }
        }
    }
}
