﻿using SpiderRobot.Domain.Entities;

namespace SpiderRobot.Domain.Model
{
    public interface ISpiderWallViewModel
    {
        string MaxCoordinates { get; set; }
        string CurrentLocation { get; set; }
        string InstructionsText { get; set; }
        string Output { get; set; }
    }

    public class SpiderWallViewModel : ISpiderWallViewModel
    {
        public string MaxCoordinates { get; set; }
        public string CurrentLocation { get; set; }
        public string InstructionsText { get; set; }
        public string Output { get; set; }
        
    }
}