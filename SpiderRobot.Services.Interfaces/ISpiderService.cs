using SpiderRobot.Domain.Entities;
using SpiderRobot.Domain.Model;

namespace SpiderRobot.Services.Interfaces
{
    public interface ISpiderService
    {
        string Navigate();
        SpiderWallViewModel SpiderWallViewModel { get; set; }
    }
}