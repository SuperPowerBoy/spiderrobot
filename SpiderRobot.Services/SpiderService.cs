﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpiderRobot.Domain.Entities;
using SpiderRobot.Domain.Model;
using SpiderRobot.Services.Interfaces;

namespace SpiderRobot.Services
{
    public class SpiderService : ISpiderService
    {
        public SpiderWallViewModel SpiderWallViewModel { get; set; }
        private readonly Spider _spider;
        private readonly Wall _wall;

        public SpiderService(Spider spider, Wall wall)
        {
            _spider = spider;
            _wall = wall;
        }

        public string Navigate()
        {
            var maxCoordinates = SpiderWallViewModel.MaxCoordinates.Split(' ');
            if (maxCoordinates.Length == 2)
            {
                _wall.XMaxCoordinate = int.Parse(maxCoordinates[0]);
                _wall.YMaxCoordinate = int.Parse(maxCoordinates[1]);
            }
            else
            {
                throw new Exception("Wrong format input");
            }

            if (SpiderWallViewModel.CurrentLocation.Split(' ').Length == 3)
            {
                _spider.XCoordinate = int.Parse(SpiderWallViewModel.CurrentLocation.Split(' ')[0]);
                _spider.YCoordinate = int.Parse(SpiderWallViewModel.CurrentLocation.Split(' ')[1]);
                _spider.Orientation = SpiderWallViewModel.CurrentLocation.Split(' ')[2].ToUpper();
            }
            else
            {
                throw new Exception("Wrong format input");
            }

            foreach (var instruction in SpiderWallViewModel.InstructionsText.ToCharArray())
            {
                Respond(instruction.ToString().ToUpper());
            }

            return String.Format("{0} {1} {2}",_spider.XCoordinate,_spider.YCoordinate,_spider.Orientation);
        }

        private void Respond(string instruction)
        {
            switch (instruction)
            {
                case "L":
                    _spider.Orientation = GetOrientation(_spider.Orientation, "L");
                    break;
                case "R":
                    _spider.Orientation = GetOrientation(_spider.Orientation, "R");
                    break;
                case "F":
                    switch (_spider.Orientation)
                    {
                        case "UP":
                            if (_spider.YCoordinate < _wall.YMaxCoordinate)
                            {
                                _spider.YCoordinate++;
                            }
                            else
                            {
                                throw new Exception("Out of bounds");
                            }
                            break;
                        case "DOWN":
                            if (_spider.YCoordinate > _wall.YMinCoordinate)
                            {
                                _spider.YCoordinate--;
                            }
                            else
                            {
                                throw new Exception("Out of bounds");
                            }
                            break;
                        case "LEFT":
                            if (_spider.XCoordinate > _wall.XMinCoordinate)
                            {
                                _spider.XCoordinate--;
                            }
                            else
                            {
                                throw new Exception("Out of bounds");
                            }
                            break;
                        case "RIGHT":
                            if (_spider.XCoordinate < _wall.XMaxCoordinate)
                            {
                                _spider.XCoordinate++;
                            }
                            else
                            {
                                throw new Exception("Out of bounds");
                            }
                            break;
                    }
                    break;
            }

            
        }

        private string GetOrientation(string currentOrientation, string instruction)
        {
            string orientation = String.Empty;
            switch (instruction)
            {
                case "L":
                    switch (currentOrientation.ToUpper())
                    {
                        case "LEFT":
                            orientation = "DOWN";
                            break;
                        case "DOWN":
                            orientation = "RIGHT";
                            break;
                        case "UP":
                            orientation = "LEFT";
                            break;
                        case "RIGHT":
                            orientation = "UP";
                            break;
                    }
                    break;
                case "R":
                    switch (currentOrientation.ToUpper())
                    {
                        case "LEFT":
                            orientation = "UP";
                            break;
                        case "DOWN":
                            orientation = "LEFT";
                            break;
                        case "UP":
                            orientation = "RIGHT";
                            break;
                        case "RIGHT":
                            orientation = "DOWN";
                            break;
                    }
                    break;
            }

            return orientation;
        }
    }
}
